USE music_db;

-- Artists that has d in its name
SELECT * FROM artists WHERE name LIKE "%d%";

-- Songs that has length less than 230
SELECT * FROM songs WHERE length < 230;

-- Join albums and songs table show only album title song title and song length
SELECT albums.album_title, songs.song_name, songs.length FROM albums
	JOIN songs ON songs.album_id = albums.id;

-- Join artists and albums table show only albums that has a in its title
SELECT * FROM artists
	JOIN albums ON albums.artist_id = artists.id WHERE albums.album_title LIKE "%a%";

-- Sort albums from Z-A (Show first 4 records)
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- Join albums and songs table sort albums from Z-A
SELECT * FROM albums
	JOIN songs ON songs.album_id = albums.id ORDER BY albums.album_title DESC;